import time
# import serial
import bluetooth
from Tkinter import *

# print("searching for devices")
# nearby_devices = bluetooth.discover_devices()

# num = 0
# print("select device by useing corresponding number")

# for i in nearby_devices:
#     num += 1
#     print(num, ":", bluetooth.lookup_name(i))

# selection = input(" > ") - 1
# print("You have selected", bluetooth.lookup_name(nearby_devices))
# bd_address = nearby_devices[selection]
# port = 1


class Application(Frame):
    # communication
    #sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    def disconnect(self):
        # self.sock.close()

    #def on(self):
        data = "y"
        # self.sock.send(data)

    def off(self):
        data = "n"
        # self.sock.send(data)

    def createWidget(self):
        self.QUIT = Button(self)
        self.QUIT["text"] = "Quit"
        self.QUIT["fg"] = "red"
        self.QUIT["command"] = self.quit

        self.QUIT.pack({"side": "left"})

        self.disconnectFrom = Button(self)
        self.disconnectFrom["text"] = "disconnect"
        self.disconnectFrom["fg"] = "darkgrey"
        self.disconnectFrom["command"] = self.disconnect

        self.disconnectFrom.pack({"side": "left"})

        self.turnOn = Button(self)
        self.turnOn["text"] = "On"
        self.turnOn["fg"] = "green"
        self.turnOn["command"] = self.on

        self.turnOn.pack({"side": "left"})

        self.turnOff = Button(self)
        self.turnOff["text"] = "Turn off"
        self.turnOff["fg"] = "darkgrey"
        self.turnOff["commandc"] = self.off

        self.turnOff.pack({"side": "left"})

        def __init__(self, master=None):

            #self.sock.connect((bd_address, port))
            Frame.__init__(self, master)
            self.pack()
            self.createWidget()


# begin UI processing
root = Tk()
app = Application(master=root)
app.mainloop()
root.destroy()
