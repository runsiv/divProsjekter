import sys
import bluetooth
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QCheckBox, QGroupBox, QSlider, QWidget, QGridLayout, QMenu, QPushButton, QRadioButton, QVBoxLayout
#from PyQt5 import QtWidgets 


class Window(QWidget):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        grid = QGridLayout()
        grid.addWidget(self.LEDcontroll(), 0, 0)
        grid.addWidget(self.LEDcontroll(), 1, 0)
        grid.addWidget(self.LEDcontroll(), 2, 0)
        self.setLayout(grid)

        self.setWindowTitle("LED controller")
        self.resize(300, 400)

    def LEDcontroll(self):
        groupBox = QGroupBox("Sliders")

        slider = QSlider(Qt.Horizontal)
        slider.setFocusPolicy(Qt.StrongFocus)
        slider.setTickPosition(QSlider.TicksBothSides)
        slider.setTickInterval(255)
        slider.setSingleStep(1)

        vbox = QVBoxLayout()
        vbox.addWidget(slider)
        vbox.addStretch(1)
        groupBox.setLayout(vbox)

        return groupBox
    #def Buttons(self)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    clock = Window()
    clock.show()
    sys.exit(app.exec_())
