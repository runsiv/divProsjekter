import sys
import bluetooth
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QComboBox, QCheckBox, QGroupBox, QSlider, QWidget, QGridLayout, QMenu, QPushButton, QRadioButton, QVBoxLayout

#from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QComboBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot


class App(QWidget):

    sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    def __init__(self):
        super().__init__()
        self.title = 'LED controller'
        self.left = 10
        self.top = 10
        self.width = 320
        self.height = 240
        self.initUI()
        #self.sock.connect((bd_addr, port))

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # dropdown menu for the bluetooth connection
        comboBox = QComboBox(self)
        nearby_devices = bluetooth.discover_devices()
        for i in nearby_devices:
            comboBox.addItem(i)
        comboBox.activated[str].connect(self.btConnect)

        # slider for chancing the pwm value
        slider = QSlider(Qt.Horizontal, self)
        slider.setObjectName("Red")

        slider.setFocusPolicy(Qt.StrongFocus)
        slider.setTickPosition(QSlider.TicksBothSides)
        slider.setMinimum(0)
        slider.setMaximum(255)
        slider.setSingleStep(25)
        slider.move(100, 200)
        slider.valueChanged.connect(self.redValue, slider.value())



        buttonOn = QPushButton("Turn on", self)
        buttonOn.move(100,70)
        buttonOn.clicked.connect(self.turnOn)

        buttonOff = QPushButton("Turn off", self)
        buttonOff.move(100, 140)
        buttonOff.clicked.connect(self.turnOff)
        print(self.sock.recv(1024))

        self.show()

    def btConnect(self, bd_addr):
        self.sock.connect((bd_addr, 1))
        self.sock.settimeout(1.0)
        print("Connected to :", bd_addr)

    def turnOn(self):
        data = 1
        self.sock.send(str(data).encode())
        print("y")


    def turnOff(self):
        data = 0
        self.sock.send(str(data).encode())
        print("n")


    def redValue(self, value):
        red = value + 1000
        print(str(red))
        self.sock.send(str(red).encode())
        data = self.sock.recv(1024)
        print(data)




if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())